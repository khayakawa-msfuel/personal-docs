# Installing Ubuntu 22.04 Jammy Jellyfish
1. Have a blank USB thumb drive ready
1. Download Ubuntu ISO
   1. https://releases.ubuntu.com/22.04/ubuntu-22.04-desktop-amd64.iso
1. Use a tool such as [Etcher](https://www.balena.io/etcher/) or [Rufus](https://rufus.ie/en/) to [create a bootable thumb drive from the iso](https://linuxhint.com/create_bootable_linux_usb_flash_drive/).
1. Boot the Dell from Thumb Drive
   1. Press F12 at while BIOS is loading to enable BIOS boot menu
1. Follow the GUI guide to finish installing Ubuntu.
   1. There are step-by-step guides on the internet such as [this](https://itsfoss.com/install-ubuntu/).

# Installing First Piece of Software from Downloaded DEB package using GUI
1. This will install Microsoft Teams client
1. Download the official package from [The Microsoft Site](https://www.microsoft.com/en-us/microsoft-teams/download-app#allDevicesSection)
    1. Select the Linux DEB (64-bit) option
1. Open the file manager, and navigate to the directory where you saved the downloaded file.
1. Right-click on the teams_[version]_amd64.deb file, and choose Properties. ![File Properties](img/FileProperty.png)
1. Switch to the "Open With" tab. Select Software Install, and click on "Set as default". Close the dialog box. ![Open With](img/OpenWith.png)
1. Double-click on the package, and complete the installation.

# Installing First Piece of Software from Downloaded DEB package using CLI (command line interface)
1. This will install Microsoft Teams client
1. Download the official package from [The Microsoft Site](https://www.microsoft.com/en-us/microsoft-teams/download-app#allDevicesSection)
    1. Select the Linux DEB (64-bit) option
1. `sudo apt install -y ./teams_[version]_amd64.deb`

# Installing Other Required Software
1. Install these pre-requisit packages
   ~~~
   sudo apt install -y \
       ca-certificates \
       curl \
       gnupg \
       lsb-release \
       apt-transport-https
   ~~~
1. Add third party software repo
    1. Docker repo
        1. `sudo mkdir -p /etc/apt/keyrings`
        1. `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg`
        1. `echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null`
    1. Kubernetes repo
        1. `sudo curl -fsSLo /etc/apt/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg`
        1. `echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list`
    1. VPN repo
        1. `sudo add-apt-repository ppa:nm-l2tp/network-manager-l2tp`
    1. Helm repo
        1. `curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /etc/apt/keyrings/helm.gpg > /dev/null`
        1. `echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list`
1. Install software
   1. `sudo apt update`
   1. Use the following to install everything
      ~~~
      sudo apt install -y \
          docker-ce docker-ce-cli containerd.io \
          docker-compose-plugin kubectl \
          net-tools awscli ack telnet network-manager-l2tp \
          network-manager-l2tp-gnome jq helm
      ~~~
    1. `sudo snap install yq`
    1. `sudo snap install kubectx --classic`
1. Install Docker Desktop
    1. Download Docker Desktop for Linux from [here](https://docs.docker.com/desktop/release-notes/)
    1. Select DEB package
    1. Install the downloaded DEB package
        1. You can double-click on the deb file, or...
        1. `sudo apt install -y ./docker-desktop-[version]-amd64.deb`
1. Install DBeaver
    1. `sudo snap install dbeaver-ce`
1. Install Apache Directory Studio (LDAP Client)
    1. `sudo apt install -y openjdk-18-jre`
    1. https://directory.apache.org/studio/download/download-linux.html
1. Install Oracle Client
    1. https://www.oracle.com/database/technologies/instant-client/linux-x86-64-downloads.html
    1. Use Version 19.15.0.0.0
    1. Download the following zip files
        1. Basic Package
        1. SQL*Plus Package
        1. Tools Package
        1. SDK Package
        1. JDBC Supplement Package
        1. ODBC Package
    1. Unzip each zip file
        1. `unzip <FileName>.zip`
    1. install the unzipped files
        1. `mv instantclient_19_15 ~`
        1. `echo 'export ORACLE_HOME=~/instantclient_19_15' | tee -a ~/.bashrc > /dev/null`
        1. `echo 'export LD_LIBRARY_PATH=${ORACLE_HOME}' | tee -a ~/.bashrc > /dev/null`
        1. `echo 'export PATH=${PATH}:${ORACLE_HOME}' | tee -a ~/.bashrc > /dev/null`
        1. `source ~/.bashrc`
    1. If you use any other shell than bash, then replace `.bashrc` in above commands with the appropriate file name
        1. zsh: `~/.zprofile`
        1. ksh: `~/.kshrc`
            1. You may need to use `VAR=value;export VAR` format
        1. tcsh: `~/.cshrc`
            1. You need to use `setenv VAR value` format
        1. fish: `~/.config/fish/config.fish`
            1. You need to use `set -gx VAR value` format

# Installing Optional Software
1. Alternative Web Browsers
    1. Google Chrome: https://www.google.com/chrome/
    1. Microsoft Edge: https://www.microsoft.com/en-us/edge
    1. Brave: https://brave.com/download/
    1. Vivaldi: https://vivaldi.com/download/
    1. Opera: https://www.opera.com/browsers
1. Terminal Emulator
    1. Terminator
        1. `sudo apt install -y terminator`
    1. rxvt
        1. `sudo apt install -y rxvt-unicode`
1. Gimp (GNU Image Manipulation Program)
    1. `sudo apt install -y gimp`
1. VSCodium (100% Open Source version of VS Code)
    1. `wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg gpg --dearmor sudo dd of=/etc/apt/keyrings/vscodium-archive-keyring.gpg`
    1. `echo 'deb [ signed-by=/etc/apt/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' | sudo tee /etc/apt/sources.list.d/vscodium.list`
    1. `sudo apt update && sudo apt install codium`
1. Console text editor
    1. vim
        1. `sudo apt install -y vim-gtk3`
        1. or an alternative version `sudo apt install -y neovim`
    1. emacs
        1. `sudo apt install -y emacs`
        1. or an alternative version `sudo apt install -y jove`
    1. joe
        1. `sudo apt install -y joe`
        1. or an alternative version `sudo apt install -y jupp`
1. SFTP client
    1. FileZilla
        1. `sudo apt install -y filezilla`
    1. lftp
        1. `sudo apt install -y lftp`
    1. nsftp
        1. `sudo apt install -y ncftp`
1. git GUI client
    1. git-gui (Very basic GUI client)
        1. `sudo apt install -y git-gui`
        1. run it with `git gui`
    1. qgit (Nice)
        1. `sudo apt install -y qgit`
    1. gittyup (Nice)
        1. `sudo apt install -y flatpak`
        1. Download the pak from https://murmele.github.io/Gittyup/
        1. `flatpak install ./Gittyup.flatpak`
        1. Save the following contents as `~/.local/share/applications/Gittyup.desktop`
           ~~~
           [Desktop Entry]
           Name=Gittyup
           Exec=flatpak run com.github.Murmele.Gittyup
           Terminal=false
           Type=Application
           Icon=/var/lib/flatpak/app/com.github.Murmele.Gittyup/x86_64/stable/2f1f5074aab5a5e677e975b2534293301bd90539856e39e7c37fb9fabfa78d1e/files/Resources/Gittyup.iconset/icon_256x256.png
           ~~~
1. diff tool
    1. `sudo apt install -y meld`
    1. [Set up meld as your git difftool and mergetool](https://stackoverflow.com/questions/34119866/setting-up-and-using-meld-as-your-git-difftool-and-mergetool)
1. Ring Central
    1. Download deb file from https://github.com/ringcentral/ringcentral-embeddable-electron-app/releases
    1. `sudo apt install -y ringcentral-embeddable-voice-app_<version>_amd64.deb`

# Setting up VPN
1. Open `Settings` (like control panel)
1. Select `Network` tab
1. Click on the `+` button for VPN
1. Select L2TP
1. Give it a name (like MSFuelcard)
1. Gateway: See the VPN Setup doc in SharePoint -> New Employee Docs
1. Type: Password
1. Username: Regular User name
1. Password: Active Directory Password
1. NT Domain: `msf`
1. Click on IPSec Settings
    1. Check `Enable IPSec Tunnel`
    1. Type: PSK
    1. Pre-Shared Key: See the VPN Setup doc
    1. Click Apply
1. Click Add

# If you prefer to use a desktop e-mail client (Evolution)
1. sudo apt install -y evolution evolution-ews
1. Start Evolution
1. It will start account set up immediately.
    1. Email Address: Your `@msfuelcard.com` email address.  
    1. Uncheck the "Look up mail server details" option. Click Next
    1. Server Type: Exchange Web Services
    1. Username: Your `@msfuelcard.com` email address
    1. Host URL: `https://outlook.office365.com/EWS/Exchange.asmx`
    1. Click Finish.
1. First the client connects to the server, it will take a while to finish synchronizing all email folders with the server.  Please be patient.
1. Your personal calendar is also available in Evolution, but not the team calendar.  Please use Outlook Web, or Teams to view the team calendar.

# Taking a screen shot
1. Press `prtsc` (which is also the `F10` key on the current version of Dell Latitude laptop given by the company).

# Setting up Fuel Card
1. Follow the project's README
1. Before running the `run-db.sh` script for the first time, you will need to run following commands
   ~~~
   mkdir -p db/sys && mkdir -p db/app && chmod 777 db/sys db/app
   ~~~

# Enabling screen share in MS Teams, Zoom, etc
1. `sudoedit /etc/gdm3/custom.conf`
1. Uncomment the line `WaylandEnable=false` by removing the `#` character
1. Reboot

# Install the self-signed SSL certificate locally
1. This step can be performed after Fuel Card is fully set up locally
1. `sudo apt update`
1. `sudo apt install -y certutil`
1. `echo QUIT | openssl s_client -connect local.fuelcard.msts.com:32222 | sed -ne '/BEGIN CERT/,/END CERT/p' > /tmp/local.fuelcard`
1. `echo QUIT | openssl s_client -connect local.fuelcard-api.msts.com:32222 | sed -ne '/BEGIN CERT/,/END CERT/p' > /tmp/local.fuelcard-api`

# Other software Koji uses
1. `sudo apt install -y gpac ffmpeg mp3splt mpv htop duf`
1. pingu
    1. `sudo apt install -y golang`
    1. `mkdir -p ~/src`
    1. `cd ~/src`
    1. `git clone https://github.com/sheepla/pingu.git`
    1. `cd pingu`
    1. `make build`
    1. `sudo install -o root -g root -m 0755 bin/pingu /usr/local/bin/pingu`

# System maintenance
1. It's your responsibility to keep the system up to date with the latest software and OS
    1. Update software installed using APT
        1. `sudo apt update`
        1. `apt list --upgradable`
        1. `sudo apt upgrade`
    1. Update software installed using snap
        1. `sudo snap refresh`
