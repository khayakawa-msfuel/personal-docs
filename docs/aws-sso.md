~~~
*********** STEP 1 ***********

 ❯ aws configure sso
SSO session name (Recommended): my-sso
SSO start URL [None]: https://d-9067751237.awsapps.com/start
SSO region [None]: us-east-1
SSO registration scopes [sso:account:access]:
Attempting to automatically open the SSO authorization page in your default browser.
If the browser does not open or you wish to use a different device to authorize this request, open the following URL:

https://device.sso.us-east-1.amazonaws.com/

Then enter the code:

GNPQ-HGWN
There are 3 AWS accounts available to you.
Using the account ID 312198633222
The only role available to you is: business_ContributorExternal
Using the role name "business_ContributorExternal"
CLI default client Region [None]: us-east-1
CLI default output format [None]:
CLI profile name [business_ContributorExternal-312198633222]:

To use this profile, specify the profile name using --profile, as shown:

aws s3 ls --profile business_ContributorExternal-312198633222



*********** STEP 2 ***********
 ❯ aws eks update-kubeconfig --name mst-ent-bet-app-eks --profile business_ContributorExternal-312198633222
Updated context arn:aws:eks:us-east-1:312198633222:cluster/mst-ent-bet-app-eks in /Users/khayakawa/.kube/config

 ❯ aws eks update-kubeconfig --name mst-ent-dev-app-eks --profile business_ContributorExternal-312198633222
Updated context arn:aws:eks:us-east-1:312198633222:cluster/mst-ent-dev-app-eks in /Users/khayakawa/.kube/config



*********** STEP 3 ***********

Edit and copy the pre-prod profile to prod profile

[profile business_ContributorExternal-906261864798]
sso_session = my-sso
sso_account_id = 906261864798
sso_role_name = business_ContributorExternal
region = us-east-1

[profile business_ContributorExternal-312198633222]
sso_session = my-sso
sso_account_id = 312198633222
sso_role_name = business_ContributorExternal
region = us-east-1

[sso-session my-sso]
sso_start_url = https://d-9067751237.awsapps.com/start
sso_region = us-east-1
sso_registration_scopes = sso:account:access



*********** STEP 4 ***********
 ❯ aws eks update-kubeconfig --name mst-ent-prd-app-eks --profile business_ContributorExternal-906261864798
Updated context arn:aws:eks:us-east-1:312198633222:cluster/mst-ent-dev-app-eks in /Users/khayakawa/.kube/config
~~~
