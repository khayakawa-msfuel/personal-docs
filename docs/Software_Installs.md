Install homebrew
1. `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
1. `echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> /Users/khayakawa/.zprofile`
1. `eval "$(/opt/homebrew/bin/brew shellenv)"`
1. `brew update`

Install iTerm2
1. `brew install --cask iterm2`

Install Microsoft Teams (for M1 Macs only)
1. `/usr/sbin/softwareupdate --install-rosetta --agree-to-license`
1. Download and install Microsoft Teams client from https://docs.microsoft.com/en-us/microsoftteams/get-clients?tabs=Windows

Install all other software
1. `brew install --cask apache-directory-studio dbeaver-community gimp keepassxc lens postman`
1. `brew install ack aws-iam-authenticator awscli ca-certificates flux flyway git helm jq yq xz wget watch tmux telnet sqlite rlwrap python@3.9 perltidy pcre2 openssl@1.1 openjdk nvm ncurses lftp kubectx kubernetes-cli kubeseal`

Setup connections to AWS accounts
1. `mkdir ~/.aws`
1. `touch ~/.aws/credentials`
1. `chmod 600 ~/.aws/credentials`
1. Edit the credentials file so it looks like the following  
    ~~~
    [312198633222_business_ContributorExternal]
    aws_access_key_id=ABCABCD
    aws_secret_access_key=abCD+abc
    aws_session_token=ABCABCDABCABCD==

    [698569588437_business_ContributorExternal]
    aws_access_key_id=ABCABCD
    aws_secret_access_key=abCD+abc
    aws_session_token=ABCABCDABCABCD==

    [906261864798_business_ContributorExternal]
    aws_access_key_id=ABCABCD
    aws_secret_access_key=abCD+abc
    aws_session_token=ABCABCDABCABCD==
    ~~~
1. `AWS_PROFILE=312198633222_business_ContributorExternal aws eks update-kubeconfig --regio us-east-1 --name mst-ent-dev-app-eks --alias mst-ent-dev-app-eks`
1. `AWS_PROFILE=312198633222_business_ContributorExternal aws eks update-kubeconfig --regio us-east-1 --name mst-ent-bet-app-eks --alias mst-ent-bet-app-eks`
1. `AWS_PROFILE=906261864798_business_ContributorExternal aws eks update-kubeconfig --regio us-east-1 --name mst-ent-prd-app-eks --alias mst-ent-prd-app-eks`
